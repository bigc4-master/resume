
OS SYSTEMS

1. linux
	* A. gentoo
	* B. arch

2. windows

3D PRINTING

1. building and maintaining 3D printers
2. prusa slicer
3. opescad
4. basic knowledge in blender

PROGRAMING

1. vim 
2. bash scripting
3. git
4. self learning the c language
5. ssh
6. can count in binary

CONSTRUCTION

1. soudering
2. metal working
	* A. cutting and bending sheet metal
	* B. cutting and bending metal tubing
	* C. riveting

3. wood working
4. pc building
	* A. understanding how a cpu works on a hardware level

META SKILLS

1. Determined; it's the definition of my name
2. curiosity to learn new things
3. creative 
4. able to folow writtin directions and diagrams to a T 
5. I pay attention to detail.
6. p - a - t - i - e - n - t.
7. i do not get sarcasm.
